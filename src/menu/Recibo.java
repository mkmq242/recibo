package menu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Martinez Duran
 */
public class Recibo {
    
    private int id;
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private float costoKw;
    private int kwCons;
    private int status;

//Metodos constructores
//Por Omision

public Recibo(){
    this.id = 0;
    this.numRecibo = 0;
    this.fecha = "";
    this.nombre = "";
    this.domicilio = "";
    this.tipoServicio = 0; 
    this.costoKw = 0.0f;
    this.kwCons = 0;
    this.status =0;
}

public Recibo(int id,int numRecibo,String fecha, String nombre, String domicilio
        , int tipoServicio, float costoKw, int kwCons, int status){
    this.id = id;
    this.numRecibo = numRecibo;
    this.fecha = fecha;
    this.nombre = nombre;
    this.domicilio = domicilio;
    this.tipoServicio = tipoServicio; 
    this.costoKw = costoKw;
    this.kwCons = kwCons;
    this.status = status;
    }

//Copia

public Recibo(Recibo otro){
    this.id = otro.id;
    this.numRecibo = otro.numRecibo;
    this.fecha = otro.fecha;
    this.nombre = otro.nombre;
    this.domicilio = otro.domicilio;
    this.tipoServicio = otro.tipoServicio; 
    this.costoKw = otro.costoKw;
    this.kwCons = otro.kwCons;
    this.status = otro.status;
}

    //Metodo get/set
    public int getId (){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostoKw() {
        return costoKw;
    }

    public void setCostoKw(float costoKw) {
        this.costoKw = costoKw;
    }

    public int getKwCons() {
        return kwCons;
    }

    public void setKwCons(int kwCons) {
        this.kwCons = kwCons;
    }
    
    public float calcularSubto (){
        float subto = 0.0f;
        
        switch (tipoServicio){
            case 1: subto = kwCons*2.0f;break;
            case 2: subto = kwCons*3.0f;break;
            case 3: subto = kwCons*5.0f;break;
        }
        
        return subto;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = calcularSubto() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = calcularSubto() + calcularImpuesto();
        return total;
    }
    
}
