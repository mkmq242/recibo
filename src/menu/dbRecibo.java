package menu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Martinez Duran
 */
import java.sql.*;
public class dbRecibo {
    
    private String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
    private String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/marcosm?user=marcosm&password=12345";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    //constructor
    
    public dbRecibo(){
        
        try {
        Class.forName(MYSQLDRIVER);
        
        } catch (ClassNotFoundException e){
            System.out.println("Surgio un error "+ e.getMessage());
            System.exit(-1);
        }
        }
        public void conectar(){
            try{
            conexion = DriverManager.getConnection(MYSQLDB);
            }
            catch(SQLException e){
                
              System.out.println("No se logro conectar "+ e.getMessage());
            }
        
        }
        public void desconectar(){
            try{
                conexion.close();
            }
            catch (SQLException e){
                
            System.out.println("Surgio un error al desconectar "+ e.getMessage());
            }
        }
        public void insertar(Recibo re){
            conectar();
            try{
                strConsulta="INSERT INTO Recibo (numrecibo,fecha,nombre,domicilio,tipo"
                    + ",costo,consumo,status)"
                    + "VALUES (?,CURDATE(),?,?,?,?,?,?)";
               
                
                PreparedStatement pst = conexion.prepareStatement(strConsulta);
                pst.setInt(1, re.getNumRecibo());
             // pst.setString(2, re.getFecha());
                pst.setString(2, re.getNombre());
                pst.setString(3, re.getDomicilio());
                pst.setInt(4, re.getTipoServicio());
                pst.setFloat(5, re.getCostoKw());
                pst.setInt(6, re.getKwCons());
                pst.setInt(7, re.getStatus());
                
                pst.executeUpdate();
                }catch (SQLException e){
                    System.out.println("Error al insertar "+e.getMessage());
                }
            desconectar();
        }
        public void actualizar(Recibo re){
            Recibo recibo = new Recibo();
            strConsulta = "UPDATE Recibo SET fecha = CURDATE(), nombre = ?, domicilio = ?, tipo = ?"
                    + ",costo = ?,consumo = ?,status = ? WHERE numrecibo = ? and status = 0; ";
           this.conectar();
            try{
                
                PreparedStatement pst = conexion.prepareStatement(strConsulta);
                
                pst.setString(1, re.getNombre());
                pst.setString(2, re.getDomicilio());
                pst.setInt(3, re.getTipoServicio());
                pst.setFloat(4, re.getCostoKw());
                pst.setInt(5, re.getKwCons());
                pst.setInt(6,re.getStatus());
                pst.setInt(7, re.getNumRecibo());
                
                pst.executeUpdate();
                this.desconectar();
                }catch (SQLException e){
                    System.out.println("Surgio un error al actualizar "+e.getMessage());
                }
            }
        public void habilitar (Recibo re){
            
            String consulta = "";
            strConsulta = "UPDATE Recibo SET status = 0 WHERE numrecibo = ?";
            this.conectar();
            try{
                System.err.println("Se conecto");
                PreparedStatement pst = conexion.prepareStatement(strConsulta);
                
                pst.setInt(1, re.getNumRecibo());
                
                pst.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Surgio un error al habilitar: "+ e.getMessage());
            }
        }
        public void deshabilitar (Recibo re){
            strConsulta = "UPDATE Recibo SET status = 1 WHERE numrecibo = ?";
            this.conectar();
            try{
                System.err.println("Se conecto");
                PreparedStatement pst = conexion.prepareStatement(strConsulta);
                
                pst.setInt(1, re.getNumRecibo());
                
                pst.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Surgio un error al deshabilitar: "+ e.getMessage());
            }
        }
        public boolean isExiste (int numRecibo, int status){
            boolean exito = false;
            strConsulta = "SELECT * FROM Recibo WHERE numrecibo = ? and status = ?";
            this.conectar();
            try{
                PreparedStatement pst = conexion.prepareStatement(strConsulta);
                pst.setInt(1, numRecibo);
                pst.setInt(2, status);
                this.registros = pst.executeQuery();
                if(this.registros.next()){
                    exito = true;
                }
            }catch(SQLException e){
                System.err.println("Surgio un error al verificar si existe: "+ e.getMessage());
            }
            this.desconectar();
            return exito;
        }
        public Recibo buscar(int numRecibo){
            Recibo recibo = new Recibo();
            conectar();
            try{
                strConsulta = "SELECT * FROM Recibo WHERE numrecibo = ? and status = 0;";
                PreparedStatement pst = conexion.prepareStatement(strConsulta);
                
                pst.setInt(1,numRecibo);
                this.registros = pst.executeQuery();
                if(this.registros.next()){
                    
                    recibo.setId(registros.getInt("id"));
                    recibo.setNumRecibo(registros.getInt("numrecibo"));
                    recibo.setNombre(registros.getString("nombre"));
                    recibo.setDomicilio(registros.getString("domicilio"));
                    recibo.setTipoServicio(registros.getInt("tipo"));
                    recibo.setCostoKw(registros.getFloat("costo"));
                    recibo.setKwCons(registros.getInt("consumo"));
                    recibo.setFecha(registros.getString("fecha"));
                    
                }else recibo.setId(0); 
            }
            catch(SQLException e){
                System.err.println("Surgio un error al habilitar: "+ e.getMessage());
            }
            this.desconectar();
            return recibo;
        }
    }
