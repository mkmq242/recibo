package menu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Martinez Duran
 */
public class TestdbRecibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Verificando errores");
        dbRecibo re = new dbRecibo();
     
        re.conectar();
        
        Recibo recibo = new Recibo();
        
        recibo.setNumRecibo(100);
        recibo.setNombre("jose miguel");
        recibo.setDomicilio("av sol 33");
        recibo.setCostoKw(10000f);
        recibo.setKwCons(1211);
        recibo.setTipoServicio(1);
        recibo.setFecha("2023-12-08");
        
        re.actualizar(recibo);
        
        re.deshabilitar(recibo);
        if(re.isExiste(100, 0)) System.out.println("si existe");
        else System.out.println("no existe");
        re.habilitar(recibo);
        
        if(re.isExiste(100, 0)) System.out.println("si existe");
        else System.out.println("no existe");
        
        Recibo result = re.buscar(100);
        
        if(result.getId()>0){
            System.out.println(result.getNombre() + "dom" + result.getDomicilio());
        }

    }
    
}
